# Do-It-Yourself GPG Token

## What is DIYNuk for?

The DIYNuk is a simplified version of the FST-01 for hand assembly.
The goal is to provide easy access to a GPG token that can be used
by hobbyists and people with little experience in soldering surface 
mount components.

![](diynuk.png)

The FST-01 is an USB 32-bit computer using an STM32F103 processor. 
It was designed with minimalist principles, but for factory 
manufacturing given its small size.

## Design files

This directory contains all the design files for the DIYNuk token.
    
    library/ : extra libraries
    *.pretty/ : image components with the logos
    datasheets/ : PDF files for all the components
    output/ :  gerber and bill of material (BOM) files
    diynuk.zip : complete project in a zip package
    README.md : this file

This derivative project follows closely the original design, except
for the footprint of the board and its components, which are meant
to make the hardware much easier to be hand soldered (with generous 
amounts of flux).

You will need KiCad >= 5.0 to edit the files in this repository.

## Licensing terms and conditions

The FST-01 was designed by Niibe Yukata from Free Software Initiative
Japan (FSIJ). All of the design work was released as Free Software 
and Open Hardware. The author requested all derivative versions of 
FST-01 continue to be released as Open Hardware to be solely used with 
Free Software toolchains. This is what the DIYNuk does, while being 
designed for assembly by folks without access to fabrication services.

In order to fulfill the licensing condition of FST-01, the DIYNuk was
released with CERN license 1.2. It is a suitable license for this purpose
given its reciprocity conditions (which bring to the hardware space a 
key foundation of the GPL). For more information on the DIYNuk licensing, 
see LICENSE and AUTHORS files. For information about the concept of 
Open Hardware, visit: [https://www.oshwa.org/definition](https://www.oshwa.org/definition)
